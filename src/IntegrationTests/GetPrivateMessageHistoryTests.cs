﻿using HipchatApiV2;
using HipchatApiV2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests
{
    [Trait("GetPrivateMessageHistory", "")]
    public class GetPrivateMessageHistoryTests
    {

        public GetPrivateMessageHistoryTests()
        {
            HipchatApiConfig.AuthToken = TestsConfig.AuthToken;
        }

        [Fact(DisplayName = "Can get private message history")]
        public void GenerateUserAuthToken()
        {
            var client = new HipchatClient();

            var privateMessageHistory = client.GetPrivateMessageHistory(TestsConfig.ExistingUserId);

            Assert.NotNull(privateMessageHistory);
        }

    }
}
