﻿using System.Collections.Generic;
using HipchatApiV2;
using HipchatApiV2.Enums;
using Xunit;

namespace IntegrationTests
{
    [Trait("GenerateToken", "")]
    public class GenerateAuthTokenExample
    {
        //[Fact(DisplayName = "Can create a token")]
        //public void GenerateAuthToken()
        //{
        //    //insert your authId and auth Secret here
        //    const string authId = "rafaelgpo@yahoo.com.br";
        //    const string authSecret = "641498@r2";

        //    var client = new HipchatClient();

        //    var token = client.GenerateToken(GrantType.Password,
        //        new List<TokenScope> {TokenScope.SendMessage}, authId, authSecret);

        //    Assert.NotNull(token);
        //}

        public GenerateAuthTokenExample()
        {
            HipchatApiConfig.AuthToken = TestsConfig.AuthToken;
        }

        [Fact(DisplayName = "Can create a user token")]
        public void GenerateUserAuthToken()
        {
            //insert your authId and auth Secret here
            const string authId = "user.mail@test.com";
            const string authSecret = "12345678";

            var client = new HipchatClient();

            var token = client.GenerateUserToken(GrantType.Password,
                new List<TokenScope> { TokenScope.SendMessage }, authId, authSecret);

            Assert.NotNull(token);
        }

    }


}